const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
const url = require('url');
const path = require('path');
const multer = require('multer');
const dir = require('node-dir');

const db = require('./db.js')
db.sequelize.sync();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : true}));
app.set("view engine", "pug");
app.set("views", path.join(__dirname ));

// zadatak 1

app.use(express.static('stranice'));

// zadatak 2



const multerConf = {
	storage:multer.diskStorage({
		destination : function(req, file, next){
			if(fs.existsSync(__dirname + '/pdfovi')){
				next(null, './pdfovi');
			}
			else{
				fs.mkdirSync(__dirname + '/pdfovi');
				next(null, './pdfovi');
			}				
		},
	filename:function(req, file, next){
		const ext = file.mimetype.split('/')[1];
		next(null, req.body.naziv+'.'+ext);
	}
	}),
	fileFilter:function(req,file,next){
		if(!file){
			next();
		}
		var nema = true;
		const ext = file.mimetype.split('/')[1];
		if(ext != "pdf"){
			nema = false;
		}
		if(!fs.existsSync(__dirname + '/jsoni')){
			fs.mkdirSync(__dirname + '/jsoni');
		}
		fs.readdir('./jsoni', function(err,files){
			if(err){
				process.exit(1);
			}
			files.forEach(function(file){
				if(req.body.naziv == file.split('Zad.json')[0]){
					nema = false;
				}
			});
			if(nema){
				next(null, true);
			}
			else{
				next(null, false);
			}
		});
	}
}


app.post("/addZadatak", multer(multerConf).single('postavka'), function(req, res, next){
	if(req.file){
		var zad = {naziv:req.body.naziv, postavka: "/" + req.body.naziv+".pdf"};
		var jsonobj = JSON.stringify(zad);
		var x = req.body.naziv+"Zad.json";
		
		fs.writeFile('./jsoni/'+x, jsonobj, 'utf8', function(err){
			if(err) throw err;
		});	
		
		// spirala 4 - dodavanje zadatka u bazu podataka
		db.zadatak.create({naziv: req.body.naziv,  postavka: '/' + req.body.naziv + '.pdf'}).then((zad1) => {
			var data = fs.readFileSync('./pdfovi/' + zad1.naziv + '.pdf');
			res.contentType("application/pdf");
			res.send(data);
		});		
		// dodano
	}
	else{
		res.render("greska", {greska: "Postoji zadatak sa istim nazivom ili ekstenzija nije pdf.", godina:false});
	}
});




// zadatak 3

app.get('/zadatak', function(req, res){
	let parametar = req.query.naziv;	
	//spirala 4 - nalazenje zadatka u bazi i njegovo vracanje
	db.zadatak.findOne({ where: {naziv: parametar} }).then(zad1 => {
		res.sendFile(__dirname + '/pdfovi/' + zad1.naziv +'.pdf', null);	
	})
	// vraceno
	
});

// zadatak 4

app.post('/addGodina',function(req,res){
	let novired = req.body;
	let linija = novired['nazivGod']+","+novired['nazivRepVje']+","+novired['nazivRepSpi'];   
	var postoji=0;
	let tacnost=1;
	
	
	fs.readFile('godine.csv', function(err, content){
		if(fs.existsSync(__dirname+'/godine.csv') == false){
			//spirala 4 - dodavanje godine u bazu
			db.godina.create({naziv: novired['nazivGod'],  nazivRepSpi: novired['nazivRepSpi'], nazivRepVje: novired['nazivRepVje']}).then((god) => {
				fs.appendFile('godine.csv',god.naziv+','+god.nazivRepSpi+','+god.nazivRepVje,function(err){
					if(err) throw err;				
					res.sendFile(__dirname + '/stranice/addGodina.html');
				});
			});		
			// dodano
		}
		else if(fs.existsSync(__dirname+'/godine.csv') == true){
			var info = content.toString();
			var redovi = info.split("\n");
			for(var i = 0; i<redovi.length; i++){
				var kolone = redovi[i].split(",");
				var celija = {nazivGod:kolone[0], nazivRepVje:kolone[1], nazivRepSpi:kolone[2]}
				if(celija.nazivGod == novired['nazivGod']){						
					tacnost=0;	
					res.render("greska", {greska: "Unesena godina vec postoji."});   
					break;
				}
			}
			if(tacnost == true){
				//spirala 4 - dodavanje godine u bazu
				db.godina.create({naziv: novired['nazivGod'],  nazivRepSpi: novired['nazivRepSpi'], nazivRepVje: novired['nazivRepVje']}).then((god) => {					
					fs.appendFile('godine.csv','\r\n'+god.naziv+','+god.nazivRepSpi+','+god.nazivRepVje,function(err){
					if(err) throw err;				
					res.sendFile(__dirname + '/stranice/addGodina.html');
					});
				});		
				// dodano
			}
		}
		else if(err) throw err;
	});   
}); 


// zadatak 5	

app.get('/godine', function(req,res){
	// spirala 4 - dodavanje svih godina u JSON
	var noviNiz=[];
	db.godina.findAll({ attributes: ['naziv', 'nazivRepSpi', 'nazivRepVje'] }).then(godine => {
		for(var i = 0; i<godine.length; i++){
			//var celija = {godine[i].naziv, godine[i].nazivRepSpi, godine[i].nazivRepVje};
			var celija = godine[i].get({plain: true});
			noviNiz.push(celija);
		}
		res.writeHead(200, {'content-type':"application/json"});
		res.end(JSON.stringify(noviNiz));
	});
	// spirala 4 - dodane
});

// zadatak 6


// zadatak 7

app.get('/zadaci', function(req,res){
	var header = req.headers.accept;
	var vratiJSON = 0;
	var vratiXML = 0;
	var vratiCSV = 0;
	if(header.indexOf('json') != -1){
		vratiJSON = 1;
	}
	if(header.indexOf('xml') != -1){
		vratiXML = 1;
	}
	if(header.indexOf('csv') != -1){
		vratiCSV = 1;
	}
	var sadrzaj = "";
	//var zadaci = fs.readdirSync('./pdfovi/');
	
	// spirala 4 - pravljenje dokumenta sa nazivima iz baze
	db.zadatak.findAll({ attributes: ['naziv'] }).then(zadaci => {
		if (vratiJSON == 1){
			sadrzaj+='[';
			for(var i in zadaci){
				var zad = zadaci[i].naziv;
				if(i != 0) sadrzaj+=', ';
				sadrzaj+='{naziv: "';
				var duzina = zad.length;
				sadrzaj+=zad;
				sadrzaj+='" postavka: "/' + zad + '.pdf' + '"}';
			}
			sadrzaj+=']';
			res.contentType("application/json");
		}
		else if(vratiXML == 1){
			sadrzaj+='<?xml version="1.0" encoding="UTF-8"?>\n<zadaci>\n';
			for(var i in zadaci){
				var zad = zadaci[i].naziv;
				sadrzaj+='<zadatak>\n<naziv> '
				var duzina = zad.length;
				sadrzaj+=zad;
				sadrzaj+=' </naziv>\n<postavka> /';
				sadrzaj+= zad + '.pdf';
				sadrzaj+=' </postavka>\n</zadatak>\n';
			}
			sadrzaj+='</zadaci>';
			res.contentType("application/xml");
		}
		else if(vratiCSV == 1){
			for(var i in zadaci){			
				var zad = zadaci[i].naziv;
				var duzina = zad.length;
				sadrzaj+=zad;
				sadrzaj+=', /';
				sadrzaj+=zad + '.pdf';
				sadrzaj+='\n';
			}
			res.contentType("text/csv");
		}
		res.send(sadrzaj);		
	});
	
	
	// napravljen dokument
	/*
	if (vratiJSON == 1){
		sadrzaj+='[';
		for(var i in zadaci){
			var zad = zadaci[i];
			if(i != 0) sadrzaj+=', ';
			sadrzaj+='{naziv: "';
			var duzina = zad.length;
			sadrzaj+=zad.substr(0, duzina-4);
			sadrzaj+='" postavka: "/' + zad + '"}';
		}
		sadrzaj+=']';
		res.contentType("application/json");
	}
	else if(vratiXML == 1){
		sadrzaj+='<?xml version="1.0" encoding="UTF-8"?>\n<zadaci>\n';
		for(var i in zadaci){
			var zad = zadaci[i];
			sadrzaj+='<zadatak>\n<naziv> '
			var duzina = zad.length;
			sadrzaj+=zad.substr(0, duzina-4);
			sadrzaj+=' </naziv>\n<postavka> /';
			sadrzaj+= zad;
			sadrzaj+=' </postavka>\n</zadatak>\n';
		}
		sadrzaj+='</zadaci>';
		res.contentType("application/xml");
	}
	else if(vratiCSV == 1){
		for(var i in zadaci){			
			var zad = zadaci[i];
			var duzina = zad.length;
			sadrzaj+=zad.substr(0, duzina-4);
			sadrzaj+=', /';
			sadrzaj+=zad;
			sadrzaj+='\n';
		}
		res.contentType("text/csv");
	}	
	res.send(sadrzaj);
	*/
});

// zadatak 8

// spirala 4 - zadatak 2.a 

	

app.post('/addVjezba',function(req,res){
	if(req.body.naziv == null){
		var nazivGodina = req.body.sGodine;
		var nazivVjezba = req.body.sVjezbe;
		db.vjezba.findOne({where: {naziv: nazivVjezba}}).then( vjezba1 => {
			var idvjezba1 = vjezba1['id'];
			db.godina.findOne({where: {naziv: nazivGodina}}).then( godina1 => {
				var idgodina1 = godina1['id'];
				godina1.getVjezbe().then( vjezbeUGodini => {
					if(!vjezbeUGodini.find(vjez => vjez.dataValues.id == idvjezba1)){
						godina1.addVjezbe(idvjezba1).then( veza => {							
						});					
					}
					res.sendFile(__dirname + '/stranice/addVjezba.html');
				});
			});
		});
	}
	else{
		var nazivGodina = req.body.sGodine;
		var nazivVjezba = req.body.naziv;
		var cekiran = req.body.spirala;
		var spiralni = false;
		if(cekiran == "on") spiralni = true;
		
		 db.vjezba.findOrCreate({where: {naziv: nazivVjezba}, defaults: {spirala: spiralni}}).spread((vjezbica, kreiran) => {
			if(kreiran == 1){
				db.godina.findOne({where: {naziv: nazivGodina}}).then( godina1 => {
					godina1.addVjezbe(vjezbica.id).then( veza => {});
				});
			}
			res.sendFile(__dirname + '/stranice/addVjezba.html');
		});
	}	
}); 

app.get('/godineajaxic', function(req,res){
	var noviNiz=[];
	db.godina.findAll().then(godine => {
		for(var i = 0; i<godine.length; i++){
			var celija = {id: godine[i].id, naziv: godine[i].naziv};
			noviNiz.push(celija);
		}
		res.send(noviNiz);
	});
});

app.get('/vjezbeajaxic', function(req,res){
	var noviNiz=[];
	db.vjezba.findAll().then(vjezbe => {
		for(var i = 0; i<vjezbe.length; i++){
			var celija = {id: vjezbe[i].id, naziv: vjezbe[i].naziv};
			noviNiz.push(celija);
		}
		res.send(noviNiz);
	});
});

app.get('/zadaciajaxic', function(req,res){
	var noviNiz=[];
	db.zadatak.findAll().then(zad => {
		for(var i = 0; i<zad.length; i++){
			var celija = {id: zad[i].id, naziv: zad[i].naziv};
			noviNiz.push(celija);
		}
		res.send(noviNiz);
	});
});
/*
// zadatak 2c

app.post('/vjezba/:idVjezbe/zadatak',function(req,res){
	
}); 

*/

app.listen(8080);


