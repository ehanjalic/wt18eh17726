var GodineAjax = (function(){     
    var konstruktor = function(divSadrzaj){   
        var ajax=new XMLHttpRequest();
        ajax.onreadystatechange=function() {
            if(ajax.readyState == 4 && ajax.status == 200){
                let odgovor = JSON.parse(ajax.responseText);
                let sadrzaj = '<div class="folder">';
                odgovor.forEach(function(jsonFile) {
					//if(jsonFile.nazivGod == "") continue;
                    sadrzaj = sadrzaj + '<div class="item"> <p>' + jsonFile.nazivGod + '</p> <p>Repozitorij vjezbe: ' +
                    jsonFile.nazivRepVje + '</p> <p>Repozitorij spirale:' + jsonFile.nazivRepSpi + '</p> </div>';
                });
				sadrzaj = sadrzaj + ' </div>';
                divSadrzaj.innerHTML = sadrzaj;
            }
        }
        ajax.open('GET','http://localhost:8080/godine',true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send();
        return {
            osvjezi:function(){
                ajax=new XMLHttpRequest();
                ajax.open('GET','http://localhost:8080/godine',true);
                ajax.setRequestHeader("Content-Type", "application/json");
                ajax.send();
            }         
        }     
    };
    return konstruktor; 
}());
