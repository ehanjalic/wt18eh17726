function ucitajGodine(){
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange =function(){
        if(ajax.readyState == 4 && ajax.status == 200){
            var jsonNiz = ajax.response;
            if(jsonNiz !='[]'){
                var parsiranNiz = JSON.parse(jsonNiz);
                var dodavanje='';
                for (var i = 0; i < parsiranNiz.length; i++){
                    dodavanje = '<option value='+parsiranNiz[i].id+'>'+parsiranNiz[i].naziv+'</option>';
					var godine = document.getElementsByName("sGodine");
					godine[0].innerHTML += dodavanje;
					godine[1].innerHTML += dodavanje;
                }
				 document.getElementsByName('sGodine')[1].onchange();
            }
        }
    };
    ajax.open('GET','http://localhost:8080/godineajaxic',true);
    ajax.send();
}

function ucitajVjezbe(){
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState == 4 && ajax.status ==200){
            jsonNiz = ajax.response;
            if(jsonNiz != '[]'){
                var parsiranNiz = JSON.parse(jsonNiz);
                var dodavanje='';
                for (var i = 0; i < parsiranNiz.length; i++){
                    dodavanje = '<option value='+parsiranNiz[i].id+'>'+parsiranNiz[i].naziv+'</option>';
					var vjezbe = document.getElementsByName("sVjezbe");
					vjezbe[0].innerHTML += dodavanje;
					vjezbe[1].innerHTML += dodavanje;
                }
                document.getElementsByName('sVjezbe')[1].onchange();
            }
        }
    };
    ajax.open('GET','http://localhost:8080/vjezbeajaxic',true);
    ajax.send();
}

function ucitaj(){
	ucitajGodine();
	ucitajVjezbe();
}